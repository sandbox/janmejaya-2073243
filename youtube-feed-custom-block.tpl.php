<?php
/**
 * @file
 * Default theme implementation to Youtube feed
 *
 * Variables available:
 * - $obj: The customise object available options .
 * - $is_ajax: Is this a ajax calling?
 */
?>
<?php if (empty($is_ajax)): ?>
  <div id="youtube-feed-player">
    <div class="vdo-container">
      <div id="vdo"  style="width:<?php print $obj->width; ?>px; height:<?php print $obj->height; ?>px;">
<?php endif; ?>
		
        <object width="<?php print $obj->width; ?>" height="<?php print $obj->height; ?>"><param name="movie" wmode="transparent" value="http://www.youtube.com/v/<?php print $obj->youtube_id; ?>&autoplay=<?php print $obj->autoplay; ?>&rel=0&theme=<?php print $obj->theme; ?>&loop=1&autohide=<?php print $obj->autohide; ?>&disablekb=<?php print $obj->disablekb; ?>&color=<?php print $obj->color; ?>"></param><param name="allowFullScreen" wmode="transparent" value="<?php print $obj->allowfullscreen; ?>"></param><param name="allowscriptaccess" value="always"></param><embed  wmode="transparent" src="http://www.youtube.com/v/<?php print $obj->youtube_id; ?>&autoplay=<?php print $obj->autoplay; ?>&rel=0&theme=<?php print $obj->theme; ?>&loop=1&autohide=<?php print $obj->autohide; ?>&disablekb=<?php print $obj->disablekb; ?>&color=<?php print $obj->color; ?>" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="<?php print $obj->allowfullscreen; ?>" width="<?php print $obj->width; ?>" height="<?php print $obj->height; ?>"></embed></object>
        <div class="clear">&nbsp;</div>
<?php if (empty($is_ajax)): ?>
      </div>
        <div id="prev-container"><a id="feed-prev">&laquo; Previous</a></div>
        <div id="next-container"><a id="feed-next">Next &raquo;</a></div>
    </div>
  </div>
<?php endif; ?>
