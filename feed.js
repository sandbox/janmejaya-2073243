/**
 * @file
 * jQuery implementation to Youtube feed
 *
 * Variables available:
 * - total_count: Total available video count .
 * - loading: Loading gif image path
 */

(function ($) {

  Drupal.behaviors.youtube_feed = {
    attach: function (context, settings) {
      var count = 1;
      var total_count = Drupal.settings.youtube_feed.total_count;
      var loading = Drupal.settings.youtube_feed.loading;
      var elem = document.createElement("img");
      elem.setAttribute("src", loading);
      elem.setAttribute("style", 'margin-top: 50%; margin-left: 50%;');
	 
      $('#feed-next').hide();
      $('#feed-prev', context).click(function () {
        (count!=total_count)?count++:count = total_count;
        if(count==total_count) {
          $('#feed-prev').hide();
          $('#feed-next').show();
        }
        else {
          $('#feed-next').show();
		}
        var nextFeed = function(data) {
          $("#vdo").html(data);
        }
        $("#vdo").html(elem);
		   
        $.ajax({
          type:'POST',
          url:'youtube',
          success:nextFeed,
          data: 'cnt='+count,
        });
        return false;
      });
	  
      $('#feed-next', context).click(function () {
        (count!=1)?count--:count = 1;
        if(count==1) {
          $('#feed-next').hide();
          $('#feed-prev').show();
        }
        else {
          $('#feed-prev').show();
		}
        var nextFeed = function(data) {
          $("#vdo").html(data);
        }
        $("#vdo").html(elem);
		
        $.ajax({
          type:'POST',
          url:'youtube',
          success:nextFeed,
          data: 'cnt='+count,
        });
        return false;
      });
    }
  };
})(jQuery);